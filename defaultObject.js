const defaultObject=(obj,def)=>{
    if(!obj || !def){
        return{};
    }
    else{
        let defResult={};
        for(items in def){
            if(!obj[items]){
                defResult[items]=def[items]
            }
            else{
                defResult[items]=obj[items]
            }
        }
        return defResult;
    }
}
module.exports=defaultObject;