const pairs=(obj)=>{
    if(!obj){
        return [];
    }
    else{
        let pairedObj=[];
        for(items in obj){
            pairedObj.push([items,obj[items]]);
        }
        return pairedObj;
    }
}
module.exports=pairs;