const invert=(obj)=>{
    if(!obj){
        return {};
    }
    else{
        let invertedObj={};
        for(items in obj){
            invertedObj[obj[items]]=items;
        }
        return invertedObj
    }
}
module.exports=invert