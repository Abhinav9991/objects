function keys(obj){
    if(!obj){
        return []
    }
    else{
        let output = []
        for (let item in obj) {
            output.push(item);
          }
        return output;
    }
}
module.exports = keys;
